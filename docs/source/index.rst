Welcome to spiops's documentation!
==================================

spiops is a library aimed to help scientists and engineers that deal with
Solar System Geometry mainly for planetary science. More in particular is
aimed to assists the users to extend the usage of SPICE.

SPICE is an essential tool for scientists and engineers alike in the
planetary science field for Solar System Geometry. Please visit the NAIF
website  for more details about SPICE.

Contents
********

.. toctree::
   :maxdepth: 2

   documentation


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


Citing spiops
*************

If the use of spiops is used in a publication, please consider
citing spiops, SpiceyPy and the SPICE toolkit. The citation information
for SPICE can be found on the NAIF website and the citation information for
SpiceyPy in the GitHub repository.


To cite SpiceyPy use the following DOI: |Citation|

.. |Citation| image:: https://zenodo.org/badge/16987/AndrewAnnex/SpiceyPy.svg
   :target: https://zenodo.org/badge/latestdoi/16987/AndrewAnnex/SpiceyPy