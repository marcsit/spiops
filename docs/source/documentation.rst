spiops package
==============

spiops.spiops module
--------------------

.. automodule:: spiops.spiops
    :members:
    :undoc-members:
    :show-inheritance:

spiops.utils.time
-----------------

.. automodule:: spiops.utils.time
    :members:
    :undoc-members:
    :show-inheritance:
